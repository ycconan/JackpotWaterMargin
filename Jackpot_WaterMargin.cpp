#include "subgames/Jackpot_WaterMargin.h"
#include "NGLog.h"
#include "Character.h"
#include "CharManager.h"
#include "Users.h"
#include "UserManager.h"
#include "Towns.h"
#include "TownManager.h"
#include "WorldPacket.h"
#include "WSSocket.h"
#include "Message.h"
#include "tinyxml/tinyxml.h"
#include "StringConverter.h"
#include "MersenneTwister.h"
#include "NoticeManager.h"
#include "DataTransferManager.h"
#include "subgames/SGAIInterface.h"
#include "subgames/SGOpcodesEx.h"
#include "VipCardManager.h"

#include "Tools.h"
#include "GameDefine.h"
#include XMLDATA_MGR_PATH
#include LOGIC_MGR_PATH
#include LOG_MGR_PATH
#include PROTOCOLS_MGR_PATH
#include ENUM_PATH

#define JACKPOT_WATERMARGIN_TOWN		1003
#define	MAX_MARCH_NUM			6
#define FULL_SCREEN_NUMBER		15
#define MARIE_EXIT_ICON         9

using namespace AIScript;

template<typename T> void AppendExPacketString(WorldPacket *packet, String str)
{
	T ilen = (T)str.size();
	*packet << ilen;
	if (ilen > 0)
		packet->Write((const uint8 *) str.c_str(), ilen);
}

//--------------------------------------------------------
JackpotWaterMargin::JackpotWaterMargin()
{
	
}

void JackpotWaterMargin::Init()
{
	m_pTown = sTownMgr.getByHandle(JACKPOT_WATERMARGIN_TOWN);

	LoadJPWaterMarginsXml();
	LoadJackPotBountyList();
	
	// 初始库存信息
	m_nKuCunCoins = m_pTown->getUInt32Field("coins");
	m_nInitKuCunCoins = m_pTown->getUInt32Field("population_max");
	m_nLastUpdatekuCunTimer = m_pTown->getUInt32Field("reign_start_timer");
	m_nJiangChiCoins = m_pTown->getUInt32Field("town_hp");
	
	m_fInJiangChiPercent = float(m_pTown->getUInt32Field("population_org") / 100.0f);
	m_fOpenJiangChiPercent = float(m_pTown->getUInt32Field("population") / 100.0f);
	m_fExtraJiangChiPercent = float(m_pTown->getUInt32Field("town_def") / 100.0f);
	m_fJiangChiReducePercent = float(m_pTown->getUInt32Field("town_att") / 100.0f);
	
	CheckAndUpdateKunCunCoins();
}

void JackpotWaterMargin::Update()		//帧循环
{
	
}

void JackpotWaterMargin::LoadJackPotBountyList()	// 初始化加载中奖列表（从数据库读取）
{
	std::list<GameMarch> marchs;
	sChannelMgr.getMarchList(&marchs, 0, MAX_MARCH_NUM, "owner_id = %u and owner_type = %u and type = %u order by create_time desc",
								m_pTown->getHandle(),m_pTown->getTargetType(),enMarchType_Normal);
	
	std::list<GameMarch> marchs2;
	sChannelMgr.getMarchList(&marchs2, 0, MAX_MARCH_NUM, "owner_id = %u and owner_type = %u and type = %u order by create_time desc",
								m_pTown->getHandle(),m_pTown->getTargetType(),enMarchType_JackPot);
	
	std::list<stJiangChiHistory> history1,history2;
	
	std::list<GameMarch>::iterator iter1,ei1 = marchs.end();
	for(iter1 = marchs.begin(); iter1!=ei1; ++iter1)
	{
		stJiangChiHistory stHistory;
		stHistory.platform_id = (*iter1).data8;		//中奖玩家PID
		stHistory.char_id = (*iter1).src_id;		//角色ID
		stHistory.single_coins = (*iter1).data4;	//投注单注金额
		stHistory.bounty_coins = (*iter1).data1;	//中奖金额
		stHistory.bounty_mult = (*iter1).data2;		//中奖倍数
		stHistory.bounty_lineNum = (*iter1).data3;	//中奖线条数
		stHistory.bounty_time = (*iter1).data6;		//中奖时间
		stHistory.bounty_percent = 0;				//中奖比例
		stHistory.name = (*iter1).sdata1;			//中奖玩家名称
		stHistory.desc = (*iter1).sdata2;			//中奖玩家头像
		
		history1.push_back(stHistory);
	}
	
	std::list<GameMarch>::iterator iter2,ei2 = marchs2.end();
	for(iter2 = marchs2.begin(); iter2!=ei2; ++iter2)
	{
		stJiangChiHistory stHistory;
		stHistory.platform_id = (*iter2).data8;		//中奖玩家PID
		stHistory.char_id = (*iter2).src_id;		//角色ID
		stHistory.single_coins = (*iter2).data4;	//投注单注金额
		stHistory.bounty_coins = (*iter2).data1;	//中奖金额
		stHistory.bounty_mult = (*iter2).data2;		//中奖倍数
		stHistory.bounty_lineNum = (*iter2).data3;	//中奖线条数
		stHistory.bounty_time = (*iter2).data6;		//中奖时间
		stHistory.bounty_percent = 0;				//中奖比例
		stHistory.name = (*iter2).sdata1;			//中奖玩家名称
		stHistory.desc = (*iter2).sdata2;			//中奖玩家头像
		
		history2.push_back(stHistory);
	}
	
	m_mapHistory.insert(std::make_pair(enMarchType_Normal,history1));		//普通游戏记录
	m_mapHistory.insert(std::make_pair(enMarchType_JackPot,history2));		//JP中奖记录
}

//--------------------------------------------------------
void JackpotWaterMargin::LoadJPWaterMarginsXml(void)
{
 	TiXmlDocument doc("../scripts_xml/subgames/Jackpot_WaterMargin.xml");
	if(!doc.LoadFile())
	{
		Log.Error("Jackpot_WaterMargin Test", "load Jackpot_WaterMargin.xml failed!");
		return;
	}
	
	TiXmlElement* pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		Log.Error("Jackpot_WaterMargin Test", "Jackpot_WaterMargin.xml have not root node!");
		return;
	}
	
	TiXmlElement* pNode = pRoot->FirstChildElement();
	while(pNode != NULL)
	{
		if (strcmp(pNode->Value(), "line") == 0)
		{// 线的点位配置
			stLine line;
			line.index = StringConverter::parseUnsignedInt(pNode->Attribute("index"));
			
			std::vector<String> vecStr = StringUtil::split(pNode->Attribute("line_pos"),",");
			std::vector<String>::iterator iter,ei = vecStr.end();
			for(iter = vecStr.begin(); iter!=ei; ++iter)
			{
				line.vPos.push_back(StringConverter::parseUnsignedInt(*iter));
			}
			
			m_mapLine.insert(std::make_pair(line.index,line));
		}
		else if (strcmp(pNode->Value(), "bounty_group") == 0)
		{// 每个位置开奖图标配置
			
			stBountyIconID bID;
			//获取图案相应配置信息
			bID.pos = StringConverter::parseUnsignedInt(pNode->Attribute("pos"));
			bID.hero_id = StringConverter::parseUnsignedInt(pNode->Attribute("hero_id"));
			bID.apera_percent = StringConverter::parseUnsignedInt(pNode->Attribute("apera_percent"));
			
			std::map<uint32,stBountyGroup>::iterator fit = m_mapBountyGroup.find(bID.pos);
			if(fit == m_mapBountyGroup.end())
			{
				stBountyGroup stBG;
				stBG.icons.push_back(bID);
				stBG.total_percent = bID.apera_percent;
				
				m_mapBountyGroup.insert(std::make_pair(bID.pos,stBG));
			}
			else
			{
				fit->second.icons.push_back(bID);
				fit->second.total_percent += bID.apera_percent;
			}
			
		}
		else if (strcmp(pNode->Value(), "bounty_mult") == 0)
		{// 中奖倍数配置
			
			uint32 icon_id = StringConverter::parseUnsignedInt(pNode->Attribute("id"));
			uint32 num = StringConverter::parseUnsignedInt(pNode->Attribute("num"));
			uint32 mult = StringConverter::parseUnsignedInt(pNode->Attribute("multiple"));
			
			std::map<uint32,std::map<uint32,uint32> >::iterator fit = m_mapBountyMult.find(icon_id);
			if(fit == m_mapBountyMult.end())
			{
				std::map<uint32,uint32> mapMult;
				mapMult.insert(std::make_pair(num,mult));
				m_mapBountyMult.insert(std::make_pair(icon_id,mapMult));
			}
			else
			{
				fit->second.insert(std::make_pair(num,mult));
			}
		}
		else if (strcmp(pNode->Value(), "marie_mult") == 0)
		{ //小玛丽对应图标奖励倍数

			uint32 icon_id = StringConverter::parseUnsignedInt(pNode->Attribute("hero_id"));
			uint32 mult = StringConverter::parseUnsignedInt(pNode->Attribute("multiple"));
			std::map<uint32,uint32>::iterator fit = m_mapMarieMult.find(icon_id);
			if (fit == m_mapMarieMult.end())
			{
				m_mapMarieMult.insert(std::make_pair(icon_id, mult));
			}
			else
			{
				fit->second.insert(std::make_pair(icon_id, mult));
			}
		}
		else if (strcmp(pNode->Value(), "mariesame_mult") == 0)
		{ //小玛丽对应图标奖励倍数

			uint32 num = StringConverter::parseUnsignedInt(pNode->Attribute("num"));
			uint32 mult = StringConverter::parseUnsignedInt(pNode->Attribute("multiple"));
			std::map<uint32, uint32>::iterator fit = m_mapMarieSameMult.find(num);
			if (fit == m_mapMarieSameMult.end())
			{
				m_mapMarieSameMult.insert(std::make_pair(num, mult));
			}
			else
			{
				fit->second.insert(std::make_pair(num, mult));
			}
		}
		else if (strcmp(pNode->Value(), "mariesame_mult") == 0)
		{ //小玛丽对应图标概率
			stBountyIconID bID;
			bID.hero_id = StringConverter::parseUnsignedInt(pNode->Attribute("hero_id"));
			bID.apera_percent = StringConverter::parseUnsignedInt(pNode->Attribute("apera_percent"));
			std::vector<stBountyGroup>::iterator fit = m_Mariegroup.find(num);
			if (fit == m_Mariegroup.end())
			{
				stBountyGroup stBG;
				stBG.icons.push_back(bID);
				stBG.total_percent = bID.apera_percent;
				m_Mariegroup.push_back(stBG);
			}
			else
			{
				fit->icons.push_back(bID);
				fit->total_percent += bID.apera_percent;
			}
		}

		else if(strcmp(pNode->Value(), "chipin_line") == 0)
		{
			m_vLines.push_back(StringConverter::parseUnsignedInt(pNode->Attribute("num")));
		}
		else if(strcmp(pNode->Value(), "chipin_signle_coins") == 0)
		{
			m_vChipSignCoins.push_back(StringConverter::parseUnsignedInt(pNode->Attribute("num")));
		}
		
		pNode = pNode->NextSiblingElement();
	}

	Log.Debug("Jackpot_WaterMargin Test", "Load Jackpot_WaterMargin.xml sucess line size[%u] bounty_group size[%u] bounty_mult size[%u]!",
			  m_mapLine.size(), m_mapBountyGroup.size(), m_mapBountyMult.size());
}

// 开始游戏
uint8 JackpotWaterMargin::ConnectionGameStart(CharPtr &pChr, const uint8 &line, const uint32 &use_coins)
{
WGS_JPWATERMARGIN_INNER_LOCK
	m_MarieGameStart = false;
	m_MarieCount = 0;
	if(pChr.isNull())
		return enStartGameResult_Failed;
		
	AICreatureInterface * pChrAI = TO_CREATURE_INTERFACE(pChr->getAIInterface());
	if(!pChrAI)
		return enStartGameResult_Failed;
	
	Log.Debug("JackpotWaterMargin::StartGame","char[%u] line[%u] use_coins[%u]",pChr->getHandle(),line,use_coins);
	
	uint8 result = enStartGameResult_Sucess;
	uint32 time_now = time(0);
	do
	{
		if(pChrAI->GetLastLineLaBaTimer() > time_now)
		{// 过于频繁的操作
			result = enStartGameResult_CD;
			break;
		}
		
		if(!CheckChipInSingleCoinsOk(use_coins))
		{// 错误的下注金额
			result = enStartGameResult_ErrorChipinCoins;
			break;
		}
		
		if(!CheckChipinLinesOk(line))
		{// 错误的线条数量
			result = enStartGameResult_ErrorChooseLine;
			break;
		}
		
		uint32 need_coins = line * use_coins;
		if(!sGLMgr.reduceItemNum(pChr,ItemModel_Coins,need_coins))
		{// 铜钱不足
			result = enStartGameResult_CoinsNeed;
			break;
		}
		
		if(use_coins >= 100)
		{// 单注100以上的可以将一定比例的钱加入奖池
			
			uint32 inCoins = uint32(m_fInJiangChiPercent * need_coins);
			Log.Debug("JackpotWaterMargin::StartGame","inCoins[%u] need_coins[%u] m_fInJiangChiPercent[%f]",inCoins,need_coins,m_fInJiangChiPercent);
			IncreaseJiangChiCoins(inCoins);
		}
		
		// 扣钱记录日志
		sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_LineFruits, ItemModel_Coins, en_UNT_Reduce, need_coins,line,use_coins);
		// 增加库存值
		IncreaseKuCunCoins(need_coins);
	}
	while(0);
	m_stBountyInfo.clear();
	std::map<uint32,std::vector<stBountyInfo> > mapBI;
	std::map<uint32,std::map<uint32,uint32> > mapOpenIcons;
	if(result == enStartGameResult_Sucess)
	{// 成功，处理接下来的逻辑
		
		// 收入url串
		uint32 agent_id = 0;
		uint32 gz_id = 0;
		uint32 pid = 0;
		UserPtr pUser = sUserMgr.load(pChr->getUInt32Field("userid"));
		if(!pUser.isNull())
		{
			agent_id = pUser->getUInt32Field("reg_from");
			gz_id = pUser->getUInt32Field("gz_id");
			pid = pUser->getUInt32Field("platform_id");
		}
		
		if(agent_id == 5031)
		{// 增加无限贷收益流水
			stIncomeInfo stICInfo;
		
			stICInfo.game_id = sGLMgr.GetTargetServerGameid(gz_id);
			stICInfo.gz_id = gz_id;
			stICInfo.channel_id = JACKPOT_FRUITS_TOWN;
			stICInfo.end_time = sGLMgr.GetCurDateTime();
			stICInfo.agent_id = agent_id;
			
			stIncomePlayerInfo playInfo;
			
			playInfo.pid = pid;
			playInfo.model_id = ItemModel_Coins;
			playInfo.num = uint32(line * use_coins);
			stICInfo.vPlayers.push_back(playInfo);
			
			sGLMgr.AddPlayerChipinIncomeInfoToUrl(stICInfo);
		}
		
		// 检查初始以及衰减库存
		CheckAndUpdateKunCunCoins();
		
		// 还差库存不够输的情况重新随机结果
		
		uint8 bounty_index = 1;
		uint32 free_count = GameResult(pChr,line,use_coins,&mapBI,false,bounty_index,&mapOpenIcons);
		if(mapBI.size())
		{// 有中奖
			
			uint32 bounty_coins = GetBountyCoins(mapBI);
			Log.Debug("JackpotWaterMargin::StartGame 111","mapBI size[%u] bounty_coins[%u] m_nKuCunCoins[%u]",
						mapBI.size(),bounty_coins,m_nKuCunCoins);
			if(m_nKuCunCoins < bounty_coins)
			{
				for(uint8 i=0; i<10; ++i)
				{
					bounty_coins = GetBountyCoins(mapBI);
					if(m_nKuCunCoins < bounty_coins)
					{// 库存铜钱不够输
						Log.Debug("JackpotWaterMargin::StartGame 222","mapBI size[%u] bounty_coins[%u] m_nKuCunCoins[%u]",
									mapBI.size(),bounty_coins,m_nKuCunCoins);
						
						mapBI.clear();
						mapOpenIcons.clear();
						
						bounty_index = 1;
						free_count = GameResult(pChr,line,use_coins,&mapBI,false,bounty_index,&mapOpenIcons);
					}
					else
					{// 库存机制生效 结束游戏
						break;
					}
				}
			}
			m_stBountyInfo = mapBI;
			WorldPacket packet;
			std::map<uint32, std::vector<stBountyInfo> >::iterator itBI, eiBI = m_stBountyInfo.end();
			for (itBI = m_stBountyInfo.begin(); itBI != eiBI; ++itBI)
			{
				uint32 total_getCoins = 0;
				uint32 total_mult = 0;
				std::vector<stBountyInfo>::iterator iter, ei = itBI->second.end();
				for (iter = itBI->second.begin(); iter != ei; ++iter)
				{
					total_mult += (*iter).mult;
					total_getCoins += (*iter).single_coins * (*iter).mult;
					Log.Debug("JackpotWaterMargin::StartGame", "single_coins[%u] combo_icon[%u] mult[%u]",
							  (*iter).single_coins, (*iter).combo_icon, (*iter).mult);
				}
			}
			m_nCurrCoins = total_getCoins;
			m_MarieCount = MarieQualification(pChr);
			if (m_MarieCount != 0)
				m_MarieGameStart = true;
		}
		
		// 成功设置刷新CD时间
		pChrAI->SetLastLineLaBaTimer(time_now + (free_count + 1) * 2);
	}
	
	// 发送对应结果给客户端
	WorldPacket packet;
	if(CreateJackpotWaterMarginGameResultPacket(&packet,mapBI,mapOpenIcons,result))
		sGLMgr.SendProtocolsToChr(pChr,&packet);
	
	SendJiangChiCoins(pChr);
	
	// 保存库存和奖池数值
	m_pTown->SaveDB();
	
	return result;
}
// 比倍or收分
uint8 JackpotWaterMargin::GradingOrDoubly(CharPtr &pChr, const uint32 &choice)
{
	if (pChr.isNull())
		return 0;
	WorldPacket packet;
	if (choice = enDoublyChoice_No)
	{//收分

		if (m_nCurrCoins)
		{
			// 减少库存值
			ReduceKuCunCoins(m_nCurrCoins);
			// 客户端动画完成后请求最新财富
			sGLMgr.addItemNum(pChr, ItemModel_Coins, m_nCurrCoins, false);
			// 记录日志
			sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_LineFruits, ItemModel_Coins, en_UNT_Add, m_nCurrCoins, total_mult);
		}
			if (CreateGradingOrDoublyPacket(&packet, choice, m_nCurrCoins))
				sGLMgr.SendProtocolsToChr(pChr, &packet);
	}
		return choice;
}
//猜大小
uint8 JackpotWaterMargin::GuessTheSize(CharPtr &pChr, const uint8 & result)
{
	if (pChr.isNull())
		return 0;
	WorldPacket packet;
	uint32 multiple, acresults, resulttype;
	// 丢骰子
	uint8 dices1 = RandomUInt(1, 6);
	uint8 dices2 = RandomUInt(1, 6);
	uint8 dices = (dices1 + dices2);

	if(dices<=6 && dices>=2)
		acresults = enGuessTheSize_Small;
	else if(dices = 7)
		acresults = enGuessTheSize_Middle;
	else
		acresults = enGuessTheSize_Large;
	
	if(result != acresults)	
	{//未中
		resulttype = 0;
		m_nCurrCoins = 0;
	}
	else
	{//中
		if (result = !enGuessTheSize_Middle)
		{
			multiple = 2;
			if (dices1 = dices2)
				multiple = 4;
		}
		else
			multiple = 6;

		resulttype = 1;
		//奖金增加
		m_nCurrCoins *= multiple;
	}
	//发送输赢结果
	if (CreateGuessSizeResultPacket(&packet, dices1, dices2, dices, resulttype, m_nCurrCoins, m_MarieGameStart))
		sGLMgr.SendProtocolsToChr(pChr, &packet);
}

//获取小玛丽游戏资格
uint8 JackpotWaterMargin::MarieQualification(CharPtr &pChr)
{
	if (pChr.isNull())
		return 0;
	std::map<uint32, std::vector<stBountyInfo> >::iterator itBI, eiBI = m_stBountyInfo.end();
	for (itBI = m_stBountyInfo.begin(); itBI != eiBI; ++itBI)
	{
		uint8 marie_count3 = 0;
		uint8 marie_count4 = 0;
		uint8 marie_count5 = 0;
		uint8 marie_count15 = 0;
		std::vector<stBountyInfo>::iterator iter, ei = itBI->second.end();
		for (iter = itBI->second.begin(); iter != ei; ++iter)
		{
			if ((*iter).combo_count = 3)
				++marie_count3;
			if ((*iter).combo_count = 4)
				++marie_count4;
			if ((*iter).combo_count = 5)
				++marie_count5;
			if ((*iter).combo_count = FULL_SCREEN_NUMBER)
				++marie_count15;
		}
	}
	uint8 Marie_count = marie_count3 * 1 + marie_count4 * 2 + marie_count5 * 3 + marie_count15 * 27;

	return Marie_count;
}

//小玛丽游戏
uint8 JackpotWaterMargin::PetiteMarie(CharPtr &pChr , const uint32 & coins)
{
	if (pChr.isNull())
		return 0;
	WorldPacket packet;
	std::map<uint32, uint32> mapIcons = CreateGameIcons(true); // 每个位置开出的个图标
	uint8 combo_num = 0;									   // 连续数量
	uint8 combo_iconID = 0;	// 中奖图标
	bool isCoincide = false;//是否中奖
	stBountyInfo bi;
	uint8 rand_id = RandomGameIcons();
	for (uint32 i = 1; i < 5; ++i)
	{
		uint8 curr_icon = mapIcons[i];
		if (combo_iconID == 0)
		{ // 第一个图标
			combo_iconID = curr_icon;
			++combo_num;
		}
		if (combo_iconID == curr_icon)
		{//连续
			combo_iconID = curr_icon;
			++combo_num;
		}
		if (curr_icon == rand_id)
			isCoincide = true;
	}

	//小玛丽奖励
	uint32 mult1, mult2;
	if(combo_num >= 3)
		mult1 = GetIconMarieMult(combo_num, false);
	if (isCoincide)
		mult2 = GetIconMarieMult(rand_id,true);
	//外圈图标为EXIT时，次数减一	
	if (rand_id = MARIE_EXIT_ICON)
		m_MarieCount-=1;

	uint32 curcoins = (mult1 + mult2) * coins;	
	//发送每轮结果
	if (CreateMarieGameRoundPacket(&packet, mapIcons, rand_id, mult1, mult2, m_MarieCount, curcoins))
		sGLMgr.SendProtocolsToChr(pChr, &packet);
	if(!m_MarieCount)

}
//随机产生外圈图标
uint8 JackpotWaterMargin::RandomGameIcons()
{
	uint8 icon_id = 0;
	stBountyGroup stBG = m_Mariegroup;
	uint32 rand_num = RandomUInt(stBG.total_percent);
	uint32 offset_num = 0;
	std::vector<stBountyIconID>::iterator itIcon, eiIcon = stBG.icons.end();
	for (itIcon = stBG.icons.begin(); itIcon != eiIcon; ++itIcon)
	{
		offset_num += (*itIcon).apera_percent;
		if (offset_num >= rand_num)
		{
			icon_id = (*itIcon).Hero_id;
			break;
		}
	}
	return icon_id;
}

// 发送奖池金额
void JackpotWaterMargin::SendJiangChiCoins(CharPtr & pChr)
{
	if(pChr.isNull())
		return ;
	
	WorldPacket packet;
	if(CreateJiangChiCoinsPacket(&packet,m_nJiangChiCoins))
		sGLMgr.SendProtocolsToChr(pChr,&packet);
		return ;
}

// 获得中奖金额
uint32 JackpotWaterMargin::GetBountyCoins(std::map<uint32,std::vector<stBountyInfo> > bounties)
{
	uint32 bounty_coins = 0;
	if(bounties.size() == 0)
		return bounty_coins;
	
	std::map<uint32,std::vector<stBountyInfo> >::iterator itBC,eiBC = bounties.end();
	for(itBC = bounties.begin(); itBC!=eiBC; ++itBC)
	{
		std::vector<stBountyInfo>::iterator itBInfo,eiBInfo= itBC->second.end();
		for(itBInfo = itBC->second.begin(); itBInfo!=eiBInfo; ++itBInfo)
		{
			bounty_coins += ((*itBInfo).single_coins * (*itBInfo).mult);
		}
	}
	
	return bounty_coins = 0;
}

// 检查下注单注金额
bool JackpotWaterMargin::CheckChipInSingleCoinsOk(const uint32 & coins)
{
	std::vector<uint32>::iterator iter,ei = m_vChipSignCoins.end();
	for(iter = m_vChipSignCoins.begin(); iter!=ei; ++iter)
	{
		if(*iter == coins)
			return true;
	}
	
	return false;
}

// 检查选择的线数量
bool JackpotWaterMargin::CheckChipinLinesOk(const uint32 & line)
{
	std::vector<uint8>::iterator iter,ei = m_vLines.end();
	for(iter = m_vLines.begin(); iter!=ei; ++iter)
	{
		if(*iter == line)
			return true;
	}
	
	return false;
}

// 产生游戏图标列表
std::map<uint32,uint32> JackpotWaterMargin::CreateGameIcons(bool isMarie)
{
	std::map<uint32,uint32> mapIcons;
	
	std::map<uint32,stBountyGroup>::iterator iter,ei = m_mapBountyGroup.end();
	for(iter = m_mapBountyGroup.begin(); iter!=ei; ++iter)
	{
		uint8 icon_id = 0;
		stBountyGroup stBG = iter->second;
		
		uint32 rand_num = RandomUInt(stBG.total_percent);
		uint32 offset_num = 0;
		std::vector<stBountyIconID>::iterator itIcon,eiIcon = stBG.icons.end();
		for(itIcon = stBG.icons.begin(); itIcon!=eiIcon; ++itIcon)
		{
			offset_num += (*itIcon).apera_percent;
			if(offset_num >= rand_num)
			{
				icon_id = (*itIcon).Hero_id;
				break;
			}
		}
	
		mapIcons.insert(std::make_pair(iter->first,icon_id));
		//小玛丽图标列表
		if(isMarie &&　iter->first =4)
			return mapIcons;
	}
	
	return mapIcons;
}

// 获得线组合
std::map<uint32,stLine> JackpotWaterMargin::GetLineGourp(const uint8 & line_count)
{
	std::map<uint32,stLine> mapLineGroup;
	for(uint8 i=0; i<line_count; ++i)
	{
		std::map<uint32,stLine>::iterator fit = m_mapLine.find(i+1);
		if(fit != m_mapLine.end())
			mapLineGroup.insert(std::make_pair(fit->first,fit->second));
	}
	
	return mapLineGroup;
}

// 得到对应图标连线中奖倍数
uint32 JackpotWaterMargin::GetIconBountyMult(const uint8 & icon_id,const uint8 & combo_count)
{
	std::map<uint32,std::map<uint32,uint32> >::iterator fit = m_mapBountyMult.find(icon_id);
	if(fit == m_mapBountyMult.end())
		return 0;
	
	std::map<uint32,uint32>::iterator fit2 = fit->second.find(combo_count);
	if(fit2 == fit->second.end())
		return 0;
	
	return fit2->second;
}
// 得到小玛丽图标中奖倍数
uint32 JackpotWaterMargin::GetIconMarieMult(const uint8 &idOrnum,bool isId)
{
	if(isId)
	{
		std::map<uint32, uint32>::iterator fit = m_mapMarieMult.find(idOrnum);
		if (fit == m_mapMarieMult.end())
			return 0;
		return fit->second;
	}
	else
	{
		std::map<uint32, uint32>::iterator fit = m_mapMarieSameMult.find(idOrnum);
		if (fit == m_mapMarieSameMult.end())
			return 0;
		return fit->second;
	}

}

// 游戏结果(返回值表示可以免费转的次数)
uint32 JackpotWaterMargin::GameResult(CharPtr & pChr,const uint8 & line,
									const uint32 & use_coins,
									std::map<uint32,std::vector<stBountyInfo> > * mapBI,
									bool bFree,const uint8 & bounty_index,
									std::map<uint32,std::map<uint32,uint32> > * mapOpenIcons)
{	
	uint32 free_count = 0;
	
	std::map<uint32,uint32> mapIcons = CreateGameIcons(false);	// 每个位置开出的个图标
	std::map<uint32,stLine> mapLine = GetLineGourp(line);	// 获得需要判断的中奖的线组合
	std::vector<stBountyInfo> bounties;						// 中奖信息
	bool bOpenJiangChi = false;
	uint8 jiangchi_lineNum = 0;
	
	std::map<uint32,stLine>::iterator iter,ei = mapLine.end();
	for(iter = mapLine.begin(); iter!=ei; ++iter)
	{// 逐条线进行中奖判断
		
		std::vector<uint8> vPos = iter->second.vPos;
			if(use_coins >= 100)
			{// 单注大于100的可以参与奖池开奖机制
				
				std::vector<uint8> vBounty_pos;
				vBounty_pos.push_back(vPos[0]);
				vBounty_pos.push_back(vPos[1]);
				vBounty_pos.push_back(vPos[2]);
				vBounty_pos.push_back(vPos[3]);
				
				// 奖池开奖标志
				bOpenJiangChi = true;
				// 开奖池线条数量
				++jiangchi_lineNum;
				
				stBountyInfo bi;
				bi.line_index = iter->first;	//中奖线ID
				bi.combo_count = 4;				//连续数量
				bi.combo_icon = enIconID_Gong;	//中奖图标ID
				bi.vBounty_pos = vBounty_pos;	//中奖图标ID
				bi.single_coins = use_coins;	//单注下注金额
				bi.mult = 1;					//中奖倍数(加上下注扣除的那一份）
				bi.bFree = bFree;				//是否免费
				bi.bArms_num = false;
				bi.bCharacter_num = false;
				
				bounties.push_back(bi);
			}
	
		else
		{// 正常中奖判断
			
			uint8 combo_num = 0;			// 连续数量
			uint8 combo_iconID = 0;			// 中奖图标
			std::vector<uint8> vBounty_pos;	// 连续点阵
			
			std::vector<uint8>::iterator itPos,eiPos = vPos.end();
			for(itPos = vPos.begin(); itPos!=eiPos; ++itPos)
			{
				uint8 curr_icon = mapIcons[*itPos];//一条线上的一个位置的图标
				if(combo_iconID == 0)
				{// 第一个图标
					combo_iconID = curr_icon;
					++combo_num;
					vBounty_pos.push_back(*itPos);
				}
				else
				{// 后面图标连续判断

					if(combo_iconID != curr_icon)
					{//不连续
						if(curr_icon == enIconID_DragonHead)
						{
							combo_iconID = curr_icon;
							++combo_num;
							vBounty_pos.push_back(*itPos);
						}
						else
						{
							combo_iconID = 0;
							combo_num = 0;
						}

					}
					else
					{//连续
						combo_iconID = curr_icon;
						++combo_num;
						vBounty_pos.push_back(*itPos);
					}
				}
			}
			
			if(combo_num >= 3)
			{// 记录中奖情况，到外面去一起结算
				uint32 mult = GetIconBountyMult(combo_iconID,combo_num);
				
				stBountyInfo bi;
				bi.line_index = iter->first;	//中奖线ID
				bi.combo_count = combo_num;		//连续数量
				bi.combo_icon = combo_iconID;	//中奖图标ID
				bi.vBounty_pos = vBounty_pos;	//中奖图标ID
				bi.single_coins = use_coins;	//单注下注金额
				bi.mult = mult;					//中奖倍数(加上下注扣除的那一份）
				bi.bFree = bFree;				//是否免费
				bi.bArms_num = false;
				bi.bCharacter_num = false;

				bounties.push_back(bi);
			}
		}

	}

	uint8 whole_num = 0;	// 连续数量
	uint8 whole_iconID = 0; // 中奖图标
	uint8 arms_num = 0;
	uint8 character_num = 0;
	stBountyInfo bi;
	for (uint32 i = 1; i < 16; ++i)
	{
		uint8 curr_icon = mapIcons[i];
		if (whole_iconID == 0)
		{ // 第一个图标
			whole_iconID = curr_icon;
			++whole_num;
		}
		if (whole_iconID == curr_icon)
		{
			//连续
			whole_iconID = curr_icon;
			++whole_num;
		}
		if (curr_icon >= 1 && curr_icon <= 3)
			++arms_num;
		if (curr_icon >= 4 && curr_icon <= 6)
			++character_num;
	}
	//全屏图
	if (whole_num = FULL_SCREEN_NUMBER)
	{
		mult = GetIconBountyMult(whole_iconID, whole_num);
		bi.line_index = 0;			  //中奖线ID
		bi.combo_count = whole_num;   //连续数量
		bi.combo_icon = whole_iconID; //中奖图标ID
		bi.vBounty_pos = 0;			  //中奖图标ID
		bi.single_coins = use_coins;  //单注下注金额
		bi.mult = mult;				  //中奖倍数(加上下注扣除的那一份）
		bi.bFree = bFree;			  //是否免费
		bi.bArms_num = false;
		bi.bCharacter_num = false;

		bounties.clear();
	}
	//only武器
	if (arms_num = FULL_SCREEN_NUMBER && whole != FULL_SCREEN_NUMBER)
	{
		bi.line_index = 0;			  //中奖线ID
		bi.combo_count = 0;			  //连续数量
		bi.combo_icon = 0; 			  //中奖图标ID
		bi.vBounty_pos = 0;			  //中奖图标ID
		bi.single_coins = use_coins;  //单注下注金额
		bi.mult = FULL_SCREEN_NUMBER; //中奖倍数(加上下注扣除的那一份）
		bi.bFree = bFree;			  //是否免费
		bi.bArms_only = true;
		bi.bCharacter_only = false;
		bounties.clear();
	}
	//only人物
	if (character_num = FULL_SCREEN_NUMBER && whole != FULL_SCREEN_NUMBER)
	{
		bi.line_index = 0;			  //中奖线ID
		bi.combo_count = whole_num;   //连续数量
		bi.combo_icon = whole_iconID; //中奖图标ID
		bi.vBounty_pos = 0;			  //中奖图标ID
		bi.single_coins = use_coins;  //单注下注金额
		bi.mult = 50;				  //中奖倍数(加上下注扣除的那一份）
		bi.bFree = bFree;			  //是否免费
		bi.bArms_only = false;
		bi.bCharacter_only = true;
		bounties.clear();
	}
	bounties.push_back(bi);

	mapBI->insert(std::make_pair(bounty_index, bounties));
	mapOpenIcons->insert(std::make_pair(bounty_index,mapIcons));
	
	// JP奖池处理
	if(bOpenJiangChi)
	{
		float fRealPercent = m_fOpenJiangChiPercent + m_fExtraJiangChiPercent * (jiangchi_lineNum - 1);
		// 奖池彩金开出
		uint32 cp_kuCunCoins = m_nJiangChiCoins;
		uint32 caijin_coins = uint32(m_nJiangChiCoins * fRealPercent);
		if(caijin_coins > 0)
		{
			sGLMgr.addItemNum(pChr,ItemModel_Coins,caijin_coins);
			// 记录日志
			sLogDataMgr.UpdateItemsInfoLog(pChr, en_ST_LineFruitsCaiJin, ItemModel_Coins, en_UNT_Add, cp_kuCunCoins);
			// 发送中奖信息
			WorldPacket packet;
			if(CreateJiangChiBountyPacket(&packet,caijin_coins))
				sGLMgr.SendProtocolsToChr(pChr,&packet);
			
			// 广播中奖公告给其他玩家
			char buf[256] = "";
			sprintf(buf,sMsg.getMessage("Jackpot_Fruits_JPBounty").c_str(),pChr->getStringField("name").c_str(),caijin_coins);
			String str = buf;
			
			sGLMgr.SendNotice(str);
			
			// 记录奖池中奖记录
			AddTOP(pChr,enMarchType_JackPot,caijin_coins,0,0,uint8(fRealPercent * 100),use_coins);
		}
		
		if(caijin_coins > 0)
		{// 扣除奖池金额
			ReduceJiangChiCoins(caijin_coins);
		}
		
	}
	
	return free_count;
}

//--------------------------------------------------------
void  JackpotWaterMargin::AddTOP(CharPtr & pChr,
							const uint8 & type,
							const uint32 & bounty_coins,
							const uint32 & bounty_mult,
							const uint8 & line,
							const uint8 & percent,
							const uint32 & signle_coins)
{
	GameMarch * pGameMarch = sChannelMgr.newDumyMarch();
	if(pGameMarch == NULL)
		return;
	
	UserPtr pUser = sUserMgr.getByHandle(pChr->getUInt32Field("userid"));
	if(pUser.isNull())
		return;	
	
	pGameMarch->status = 1;
	pGameMarch->owner_id = JACKPOT_FRUITS_TOWN;
	pGameMarch->owner_type = RTT_TOWN;
	pGameMarch->src_id = pChr->getHandle();
	pGameMarch->src_type = pChr->getTargetType();
	pGameMarch->type = type;						//记录类型（1 拉霸游戏 2 JP）
	pGameMarch->data1 = bounty_coins;				//获奖金额
	pGameMarch->data2 = bounty_mult;				//中奖倍数
	pGameMarch->data3 = line;						//选择线条数				
	pGameMarch->data4 = signle_coins;				//单注下注金额
	
	pGameMarch->data6 = time(0);					//中奖时间
	pGameMarch->data7 = percent;					//中奖比例
	
	pGameMarch->data8 = pUser->getUInt32Field("platform_id");	//角色PID
	
	pGameMarch->sdata1 = pChr->getStringField("name");			//角色昵称
	pGameMarch->sdata2 = pChr->getStringField("desc");			//角色头像
	
	pGameMarch->create_time = sTools.GetCurDateTime();
	
	stJiangChiHistory stHistory;
	stHistory.platform_id = pGameMarch->data8;		//中奖玩家PID
	stHistory.char_id = pGameMarch->src_id;			//角色ID
	stHistory.single_coins = pGameMarch->data4;		//投注单注金额
	stHistory.bounty_coins = pGameMarch->data1;		//中奖金额
	stHistory.bounty_mult = pGameMarch->data2;		//中奖倍数
	stHistory.bounty_lineNum = pGameMarch->data3;	//中奖线条数
	stHistory.bounty_percent = 0;					//中奖比例
	stHistory.bounty_time = pGameMarch->data6;		//中奖时间
	stHistory.name = pGameMarch->sdata1;				//中奖玩家名称
	stHistory.desc = pGameMarch->sdata2;				//中奖玩家头像
	
	sChannelMgr.addMarchDB(pGameMarch);
	sChannelMgr.freeDumyMarch(pGameMarch);
	
	std::map<uint8, std::list<stJiangChiHistory> >::iterator fit = m_mapHistory.find(type);
	if(fit == m_mapHistory.end())
	{
		std::list<stJiangChiHistory> vHistory;
		vHistory.push_back(stHistory);
		
		m_mapHistory.insert(std::make_pair(type,vHistory));
	}
	else
	{
		fit->second.insert(fit->second.begin(),stHistory);
		if(fit->second.size() > MAX_MARCH_NUM)
		{// 删除多余的记录
			fit->second.pop_back();
		}
	}
	
}

//--------------------------------------------------------
void JackpotWaterMargin::SendJackpotTOP(CharPtr & pChr,const uint8 & type)
{
	WGS_JPWATERMARGIN_INNER_LOCK
	if(pChr.isNull())
		return;
	
	WSSocketPtr pSocket = pChr->getSocket();
	if(pSocket.isNull())
		return;
	
	std::list<stJiangChiHistory> history;
	std::map<uint8, std::list<stJiangChiHistory> >::iterator fit = m_mapHistory.find(type);
	if(fit != m_mapHistory.end())
		history = fit->second;
	
	WorldPacket packet;
	if(CreateJackpotWaterMarginHistoryPacket(&packet, type, history))
		pSocket->SendPacket(&packet);
}

// 创建中奖记录协议包
bool JackpotWaterMargin::CreateJackpotWaterMarginHistoryPacket(WorldPacket * packet,const uint8 & type,std::list<stJiangChiHistory> history)
{
	if(!packet)
		return false;
	
	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_JPHISTORY);
	*packet << uint16(SMSG_JPNEW_JPHISTORY) << uint16(packlen)
			<< uint8(history.size());
	
	std::list<stJiangChiHistory>::iterator iter,ei = history.end();
	for(iter = history.begin(); iter!=ei; ++iter)
	{
		*packet << uint32((*iter).char_id)			//角色ID
				<< uint32((*iter).bounty_coins)		//中奖金额
				<< uint32((*iter).bounty_mult)		//中奖倍数
				<< uint32((*iter).bounty_time);		//中奖时间
		
		AppendExPacketString<uint8>(packet,(*iter).name);			//昵称
		AppendExPacketString<uint8>(packet,(*iter).desc);			//头像
		
		CharPtr pChr = sCharMgr.getByHandle((*iter).char_id);
		if(pChr.isNull())
			pChr = sCharMgr.load((*iter).char_id);
		
		if(!pChr.isNull())
		{
			sVipCardMgr.PacketVipIcon(packet,pChr);
		}
		else
		{
			*packet << uint8(0);
		}
		
	}
	
	packet->SetLength(packet->size());
	return true;
}

// 创建游戏结果协议包
bool JackpotWaterMargin::CreateJackpotWaterMarginGameResultPacket(WorldPacket * packet,
														std::map<uint32,std::vector<stBountyInfo> > mapBI,
														std::map<uint32,std::map<uint32,uint32> > mapIcons,
														const uint8 & result)
{
	if(!packet)
		return false;
	
	if(mapBI.size() != mapIcons.size())
		return false;
	
	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_STARTGAME);
	*packet << uint16(SMSG_JPNEW_STARTGAME) << uint16(packlen)
			<< uint8(result)
			<< uint8(mapBI.size());
	
	std::map<uint32,std::vector<stBountyInfo> >::iterator iter,ei = mapBI.end();
	for(iter = mapBI.begin(); iter!=ei; ++iter)
	{
		// 开奖图标数量
		std::map<uint32,std::map<uint32,uint32> >::iterator fit = mapIcons.find(iter->first);
		if(fit == mapIcons.end())
		{
			*packet << uint8(0);
		}
		else
		{
			*packet << uint8(fit->second.size());
			std::map<uint32,uint32>::iterator it2,ei2 = fit->second.end();
			for(it2 = fit->second.begin(); it2!=ei2; ++it2)
			{
				*packet << uint8(it2->first)
						<< uint8(it2->second);
			}
		}
		
		*packet << uint8(iter->second.size());
		
		uint32 total_getCoins = 0;
		std::vector<stBountyInfo>::iterator iter2,ei2 = iter->second.end();
		for(iter2 = iter->second.begin(); iter2!=ei2; ++iter2)
		{
			*packet << uint8((*iter2).line_index)			//中奖线条ID
					<< uint8((*iter2).vBounty_pos.size());	//连续数量
			
			std::vector<uint8>::iterator itPo,eiPo = (*iter2).vBounty_pos.end();
			for(itPo = (*iter2).vBounty_pos.begin(); itPo!=eiPo; ++itPo)
			{// 点阵
				*packet << uint8(*itPo);
			}
			
			*packet	<< uint32((*iter2).mult)				//中奖倍数
					<< uint8((*iter2).bFree);				//是否免费
			
			total_getCoins += ((*iter2).mult * (*iter2).single_coins);
		}
		
		*packet << uint32(total_getCoins);
	}
	
	packet->SetLength(packet->size());
	return true;
}

// 创建奖池金额信息包
bool JackpotWaterMargin::CreateJiangChiCoinsPacket(WorldPacket * packet,const uint32 & coins)
{
	if(!packet)
		return false;
	
	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_JIANGCHICOINS);
	*packet << uint16(SMSG_JPNEW_JIANGCHICOINS) << uint16(packlen)
			<< uint32(coins);
	
	packet->SetLength(packet->size());
	return true;
}


// 创建奖池中奖信息包
bool JackpotWaterMargin::CreateJiangChiBountyPacket(WorldPacket * packet,const uint32 & coins)
{
	if(!packet)
		return false;
	
	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_JIANGCHIBOUNTY);
	*packet << uint16(SMSG_JPNEW_JIANGCHIBOUNTY) << uint16(packlen)
			<< uint32(coins);
	
	packet->SetLength(packet->size());
	return true;
}

// 比倍Or收分(0x0536)
bool JackpotWaterMargin::CreateGradingOrDoublyPacket(WorldPacket *packet, const uint8 &choice, const uint32 &total_getCoins)
{
	if (!packet)
		return false;

	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_GETGRADORDOUBLY);
	*packet << uint16(SMSG_JPNEW_GETGRADORDOUBLY) << uint16(packlen)
			<< uint8(choice)
			<< uint32(total_getCoins);

	packet->SetLength(packet->size());
	return true;
}
//猜大小结果
bool JackpotWaterMargin::CreateGuessSizeResultPacket(WorldPacket *packet, const uint8 &dices1, const uint8 &dices2,
													 const uint8 &dices, const uint8 &resulttype, const uint32 &curcoins, const bool mariegamestart);
{
	if (!packet)
		return false;

	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_GETSIZESULT);
	*packet << uint16(SMSG_JPNEW_GETSIZESULT) << uint16(packlen)
			<< uint8(dices1)
			<< uint8(dices1)
			<< uint8(dices)
			<< uint8(resulttype)
			<< uint32(curcoins)
			<< mariegamestart;
	packet->SetLength(packet->size());
	return true;
}
//小玛丽每轮结果
bool JackpotWaterMargin::CreateMarieGameRoundPacket(WorldPacket *packet, std::map<uint32, uint32> mapIcons, const uint8 &rand_id, const uint32 &mult1, 
																				const uint32 & mult2,const uint8 & marieCount, const uint32 & curcoins)
{
	if (!packet)
		return false;

	uint16 packlen = 12;
	packet->clear();
	packet->SetOpcode(SMSG_JPNEW_MARIEGAMEROUND);
	*packet << uint16(SMSG_JPNEW_MARIEGAMEROUND) << uint16(packlen);

	for (uint32 i = 1; i < 5; ++i)
		*packet << uint8(mapIcons[i]);
	*packet << uint8(rand_id)
			<< uint8(mult1)
			<< uint8(mult2)
			<< uint8(marieCount)
			<< uint32(curcoins);

	packet->SetLength(packet->size());
	return true;
}

//==========================库存相关====================================
// 增加库存
uint32 JackpotWaterMargin::IncreaseKuCunCoins(const uint32 & coins)
{
	m_nKuCunCoins += coins;
	
	m_pTown->setUInt32Field("reign_start_timer",m_nKuCunCoins);
	return 0;
}

// 减少库存
uint32 JackpotWaterMargin::ReduceKuCunCoins(const uint32 & coins)
{
	m_nKuCunCoins = m_nKuCunCoins > coins ? m_nKuCunCoins - coins : 0;
	
	m_pTown->setUInt32Field("reign_start_timer",m_nKuCunCoins);
	return 0;
}

// 检查和修改库存
void JackpotWaterMargin::CheckAndUpdateKunCunCoins()
{
	time_t tn = time(0);
	
	if(m_nKuCunCoins == 0)
	{// 初始库存
		m_nKuCunCoins = m_nInitKuCunCoins;
		m_nLastUpdatekuCunTimer = tn;
		
		m_pTown->setUInt32Field("coins",m_nKuCunCoins);
		m_pTown->setUInt32Field("reign_start_timer",m_nLastUpdatekuCunTimer);
		m_pTown->SaveDB();
	}
	else
	{
		if(m_nKuCunCoins > m_nInitKuCunCoins)
		{// 库存比初始库存多，衰减
			
			time_t lT = m_nLastUpdatekuCunTimer;
			tm tnTm = *(localtime(&tn));
			tm lTTm = *(localtime(&lT));
			if(tnTm.tm_year != lTTm.tm_year || tnTm.tm_yday != lTTm.tm_yday)
			{// 不是同一天，进行库存衰减
				m_nKuCunCoins = uint32(m_nKuCunCoins * float(1 - m_fJiangChiReducePercent));
				m_pTown->setUInt32Field("coins",m_nKuCunCoins);
			}
		}
	}
	
}

//==========================库存相关====================================

//==========================奖池相关====================================
// 增加奖池金额
uint32 JackpotWaterMargin::IncreaseJiangChiCoins(const uint32 & coins)
{
	m_nJiangChiCoins += coins;
	m_pTown->setUInt32Field("town_hp",m_nJiangChiCoins);
	return m_nJiangChiCoins;
}

// 减少奖池金额
uint32 JackpotWaterMargin::ReduceJiangChiCoins(const uint32 & coins)
{
	m_nJiangChiCoins = m_nJiangChiCoins > coins ? m_nJiangChiCoins - coins : 0;
	m_pTown->setUInt32Field("town_hp",m_nJiangChiCoins);
	return m_nJiangChiCoins;
}
//==========================奖池相关====================================
